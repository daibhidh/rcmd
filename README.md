# Description

rcmd (remote command) is a command line utility written in Ruby for executing the same command on
multiple systems through ssh.  This is done by assigning a host to a
thread and creating the ssh sessions in those threads.

## Features

- Command line switch for setting number of threads to be used at a time. (Default is 4)
- A comma seperated list of hosts piped from another command can be used as the node list.
- Username can be specified.  (Default is root.  Requires valid ssh-key in ~/.ssh)
- Output shows which host the output is from
- Stdout can be suppressed, only allowing for stderr to be shown
- Usage of Regex for generating the host list
- Support for SQLite3, MySql/MariaDB and PostgreSQL queries for generating host list
- Hosts file can be specified ( /dev/null as an example)
- Keyfile can be specified
- Keys only option added to fail attempts if key authentication fails.

## Installation
rcmd is available as a gem and kan be installed with `gem install rcmd`


# Usage Examples

## Help Screen

```bash
Usage: rcmd [options]
    -u, --username username          Username for SSH connections
    -n, --nodes x,y,z                Comma seperated list of nodes. use '-' for a space seperated list piped from another command
    -r, --regexp regex               Use Regex to build host list (ruby regexp)
    -t, --threads threads            Number of threads to run
    -k, --keyfile keyfile            Keyfile to use for the connections
    -c, --command <command>          Quoted string containing the command to be run
    -q, --quiet                      Suppress stdout of commands.  stderr will still be displayed
    -v, --version                    Print what version of the command is in use
    -D, --debug                      Print debug information
        --hosts-file FILE            Filename to use as the SSH hosts file (known_hosts)
        --keys_only                  Use only ssh keys for authentication. (No Passwords)
 
Database Options
    -C, --create-config              Create template dbconfig file in ~/.rcmd
    -T, --type server-type           Database query on server type
    -H, --host hostname-pattern      Database query on hostname (sql like query)
    -O, --os operating-system        Database query on Operating System string
```

## Specifying nodes manually
Specifying a comma seperated list of hosts and timing the total execution time.
```bash 
daibhidh@darkstar:~/$ time rcmd -n node1,node2,node3,node4 -c 'cat /etc/redhat-release'
node2 :: Red Hat Enterprise Linux ComputeNode release 6.8 (Santiago)
node1 :: Red Hat Enterprise Linux ComputeNode release 6.8 (Santiago)
node4 :: Red Hat Enterprise Linux ComputeNode release 6.9 (Santiago)
node3 :: Red Hat Enterprise Linux ComputeNode release 6.9 (Santiago)


real	0m2.749s
user	0m0.272s
sys	    0m0.050s
```

## Piping list of nodes
Using the hammer cli to get a list of hosts from a RedHat Satellite and providing it to rcmd as the node list.
```bash
daibhidh@darkstar:~/$ hammer --output base host list --organization org | awk '/Name:/ {print $2}' | rcmd -n - -c 'cat /etc/redhat-release'
node1.example.com :: Red Hat Enterprise Linux ComputeNode release 6.8 (Santiago)
node3.example.com :: Red Hat Enterprise Linux ComputeNode release 6.9 (Santiago)
node2.example.com :: Red Hat Enterprise Linux ComputeNode release 6.8 (Santiago)
node4.example.com :: Red Hat Enterprise Linux ComputeNode release 6.9 (Santiago)
```

## Using regex to build node list
```bash
daibhidh@darkstar:~/$ rcmd -r 'node([1-9]|1[1-2])$' -c 'hostname -f'
node2 :: node2.example.com
node1 :: node1.example.com
node4 :: node4.example.com
node3 :: node3.example.com
node5 :: node5.example.com
node6 :: node6.example.com
node8 :: node8.example.com
node7 :: node7.example.com
node11 :: CONNECT ERROR :: Unable to connect to host!
node9 :: node9.example.com
node12 :: node12.example.com
```

## Database backend support
The database backend support is provided by ActiveRecord and thus
compatible with databases which are supported by ActiveRecord. Out of
the box mysql2, sqlite3 and postgresql work.  To use another database
backend then you will need to download the source, add it to the
rcmd.gemspec file, and correct the 'requires' in lib/rcmd/db.rb
accordingly.

I tried having all of types in there but the dependency requirements
became too great, both within Ruby and development libraries on the
system.

To set up the database backend run rcmd with the '-C' switch first.
This will create a database configuration file in ~/.rcmd named
dbconfig.yml.  In this file you can specify the adapter, and various
other soptions for the database backend.  For the queries to work
properly, the :host_field: *MUST* be set so the command knows what
column contains the host name.

Two optional fields exist as well, which are type_field and os_field.
While these are meant to represent "Server type (I.e. Web, DB, etc)
and Operating System Name/version, you can map these to whatever
fields you would like to be able to sort by.  If these fields are not
specified then using the associated command line options will result
in a run time error.

To minimize the size and possible issues with various backend
adapters, no adapters are listed/installed when installing this Gem.
It is up to you to install the correct adapter for the database you
are wanting to connect to.

The '-T' and '-O' options use strict matching in their queries where
as the '-H' option uses a 'LIKE' query.

### Database backend examples

Hostname query (Match all systems whose hostname starts with node and contains at least two additional charectors):
```bash
daibhidh@darkstar:~/$ rcmd -H 'node%_' -c 'hostname -f'
node20 :: node20.example.com
node10 :: node10.example.com
node12 :: node12.example.com
node13 :: node13.example.com
node15 :: node15.example.com
node16 :: node16.example.com
node18 :: node18.example.com
node17 :: node17.example.com
node11 :: node11.example.com
node19 :: node19.example.com
node14 :: node14.example.com
```

Type query (Match all hosts listed as web servers):
```bash
daibhidh@darkstar:~/$ rcmd -T 'web' -c 'hostname -f'
node11 :: node11.example.com
node19 :: node19.example.com
node14 :: node14.example.com
```

OS query (Match all hosts listed as running slackware):
```bash
daibhidh@darkstar:~/$ rcmd -o 'slackware' -c 'hostname -f'
node10 :: node10.example.com
node12 :: node12.example.com
node18 :: node18.example.com
node17 :: node17.example.com
node11 :: node11.example.com
```
## Database Issues

Due to some Operating systems forcing older Ruby versions (RedHat,
CentOS, Mac OS X) rcmd will not install properly on these hosts. You
can either manually install a newer version of Ruby (>= 2.2.2) or you
can try to install activerecord and activesuppert versions 4.2.6.
This might work but I have not fully tested it as of yet.

# Development

If you are wanting to modify the code by all means do so.  If you clone this repository you can then run `bundle install` to install the dependencies needed.  Tests are performed with *rspec* and can be run with *rake*. 

Available Rake tasks are:

```bash
rake build            # Build rcmd-1.6.4.gem into the pkg directory
rake clean            # Remove any temporary products
rake clobber          # Remove any generated files
rake clobber_rdoc     # Remove RDoc HTML files
rake console          # Open an IRB console with this gem loaded
rake install          # Build and install rcmd-1.6.4.gem into system gems
rake install:local    # Build and install rcmd-1.6.4.gem into system gems without network access
rake rdoc             # Build RDoc HTML files
rake reinstall        # Remove, build, and install gem
rake release[remote]  # Create tag v1.6.4 and build and push rcmd-1.6.4.gem to Rubygems
rake rerdoc           # Rebuild RDoc HTML files
rake test:all         # Run all RSpec tests
rake test:db          # Test Database functionality (sqlite3 required)
rake test:mainlib     # Test main rcmd lib
```




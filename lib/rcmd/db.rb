require "rubygems"
require "active_record"
require "fileutils"
require "yaml"

module Rcmd #:notnew:

=begin rdoc

This class provides a middle layer for allowing a database to be
queried for hosts.  This is accomplished by using ActiveRecord for the
middle layer and thus should support any database ActiveRecord
supports.  Currently however, only the following are properly
configured for backend use in this class:

Backend Databases:
- sqlite3
- mysql2
- postgresql

This is achieved in the DB.db_connect method, which matches the
specifed adapter with the appropriate 'require' call by use of a case
statement.  If you are needing/wanting to add another database to this
you will need to add your require code for the db backend there. As we
do this comparison, the various backends are not specified in the
Gemspec file as it is the responsibilty of the user to ensure the
correct gems are installed for the needed database.

This class relies on a yaml configuration file to be present and as
such the 'DB.create_config' method is provided to create an example
configuration which can be then edited.  The options in the config
file are:

---
- :adapter: sqlite3                # The adapter to be used by ActiveRecord
  :host: servername                # The dns/IP/server name that hosts the database
  :database: ":memory:"            # The database/sid name to use for the connection
  :table: servers                  # What table should be queried in the database
  :username: username              # The username for the DB connection
  :password: passowrd              # The password for the connection
  :host_field: hostname            # What column/field is used for the server/host name
  :type_field: stype               # What column/field is used for server type (if any)
  :os_field: os_type               # What column/field is used for os version (if any)

The host_field is required in order to perform any queries.  If the
os_field or type_fields are not specified then those respective
queries will not run and instead raise a RuntimeError; stating that
the configuration prevents those options.

The remaining fields are dependent on the database backend to
determine if they are needed or not.

Quick usage:

Rcmd::DB.db_connect
Rcmd::DB.query_by_{hostname|type|os}(query term)

Raw queries directly on the Rcmd::DB object can be used, to include
adding records, but this is outside the scope of the command and thus
not implemented.

=end rdoc

  class DB < ActiveRecord::Base
    
    @config_dir = File.expand_path("~/.rcmd")
    @config_file = "#{@config_dir}/dbconfig.yml"
    @conf = nil
    @db_type_queries = true
    @db_os_queries = true

    # Method for overiding the location and name of the default config
    # file (Used by RSpec tests)
    def DB.override_config_file(path)
      @config_file = path
    end
    
    # Create a base config file
    def DB.create_config
      unless File.directory?(@config_dir)
        FileUtils.mkdir_p(@config_dir)
      end
      
      conf = [ :adapter => "sqlite3", :host => nil, :database => ':memory:', :table => 'servers', :username => nil, :password => nil, :host_field => 'hostname', :type_field => 'server_type', :os_field => 'osversion' ]
      
      File.open(@config_file, 'w') do |f|
        f.write(conf.to_yaml)
      end
    end

    # Performs "LIKE" queries on the host_field (hostname)
    def DB.query_by_hostname(term) 
      return where("#{@conf[:host_field]} LIKE ?", term ).map { |r| r[@conf[:host_field]] }
    end

    # Perform direct 'Hash style' queries on the os_field
    def DB.query_by_os(term)
      unless @db_os_queries
        raise RuntimeError, "OS based queries not supported in the configuration supplied"
      end
      return where(@conf[:os_field] => term).map { |r| r[@conf[:host_field]] }
    end

    # Perform direct 'Hash style' queries on the type_field
    def DB.query_by_type(term)
      unless @db_type_queries
        raise RuntimeError, "Type queries not supported in the configuration supplied"
      end
      return where(@conf[:type_field] => term).map { |r| r[@conf[:host_field]] }
    end

    # Method called by DB.db_connect to load the yaml configuration
    # file and perform some basic sanity checks and disabling of
    # os/type queries if those fields are not in the configuration
    def DB.load_config
      unless File.file?(@config_file)
        raise RuntimeError, "No database configuration file found: #{@config_file}"
      end

      @conf = YAML.load_file(@config_file)[0]

      unless @conf[:adapter]
        raise RuntimeError, "No adapter specified for database backend"
      end
      
      unless @conf[:type_field]
        @db_type_queries = false
      end

      unless @conf[:os_field]
        @db_os_queries = false
      end

      unless @conf[:adapter].downcase == "sqlite3"
        if @conf[:host].nil?
          raise RuntimeError, "No database host server specified"
        end
        if @conf[:username].nil? || @conf[:password].nil?
          raise RuntimeError, "No DB credentials specified"
        end
      end

      if @conf[:database].nil?
        raise RuntimeError, "No database specified"
      end

      if @conf[:table].nil?
        raise RuntimeError, "Table not specified"
      end
    end

    # Method for establishing a connection to the database backend
    # according to the configuration file.  This is also where we set
    # the 'require' the need gems to interact with the chosen DB
    # adapter and set the table_name variable of the class.
    def DB.db_connect
      self.load_config

      begin
        if @conf[:adapter].downcase == "sqlite3"
          ActiveRecord::Base.establish_connection(:adapter => @conf[:adapter].downcase,
                                                  :database => @conf[:database])
        else
          ActiveRecord::Base.establish_connection(
                                                  :adapter => @conf[:adapter].downcase,
                                                  :host => @conf[:host],
                                                  :username => @conf[:username],
                                                  :password => @conf[:password],
                                                  :database => @conf[:database] )
        end
        
        @table_name = @conf[:table]
      rescue RuntimeError, AdapterNotFound, ArgumentError => e
        raise RuntimeError, "DB Error when configuring connection: #{e}"
      end
    end
  end
end

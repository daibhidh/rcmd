# coding: utf-8
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "rcmd/version"


Gem::Specification.new do |spec|
  spec.name          = "rcmd"
  spec.version       = Rcmd::VERSION
  spec.authors       = ["David Rose"]
  spec.email         = ["david.rose@starnix.se"]

  spec.summary     = "Parellel command execution through ssh"
  spec.description = "This gem provides a command to execute a command on multiple systems in parallel through ssh"
  spec.homepage    =
    'http://www.gitlab.com/daibhidh/rcmd'
  spec.license       = 'GPL2'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency('net-ssh')
  spec.add_runtime_dependency('io-console')
  spec.add_runtime_dependency('thread')
  spec.add_runtime_dependency('tty-prompt')
  spec.add_runtime_dependency('activesupport')
  spec.add_runtime_dependency('activerecord')
  spec.add_runtime_dependency('regexp-examples')
  spec.add_development_dependency('sqlite3')
  spec.add_development_dependency('rspec', '~> 3.0', '>= 2.0.0')
  spec.add_development_dependency "bundler", ">= 1.0"
  spec.add_development_dependency "rake", "~> 10.0"
end

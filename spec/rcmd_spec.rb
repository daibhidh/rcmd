require "spec_helper"

RSpec.describe Rcmd do

  #
  # Mainlib tests
  #
  
  it "has a version number", :mainlib => true do
    expect(Rcmd::VERSION).not_to be nil
  end

  it "can set host_list", :mainlib => true do
    Rcmd.host_list= ["dummy", "host"]
    expect(Rcmd.host_list).not_to be nil
    expect(Rcmd.host_list).to include("dummy")
  end

  it "can set command", :mainlib => true do
    Rcmd.command= "hostname -f"
    expect(Rcmd.command).not_to be nil
    expect(Rcmd.command).to match("hostname -f")
  end

  it "can set number of threads", :mainlib => true do
    Rcmd.nthreads= 4
    expect(Rcmd.nthreads).not_to be nil
    expect(Rcmd.nthreads).to be(4)
  end
  
  it "can set user", :mainlib => true do
    Rcmd.user= "root"
    expect(Rcmd.user).not_to be nil
    expect(Rcmd.user).to match("root")
  end

  it "can set host file", :mainlib => true do
    Rcmd.hosts_file= "spec/testconfig.yml"
    expect(Rcmd.hosts_file).not_to be nil
    expect(Rcmd.hosts_file).to match("spec/testconfig.yml")
  end

  it "can set keys_only", :mainlib => true do
    Rcmd.keys_only= true
    expect(Rcmd.keys_only).to be(true)
  end

  it "can set debug", :mainlib => true do
    Rcmd.debug= true
    expect(Rcmd.debug).to be(true)
  end

  it "can set the key_file", :mainlib => true do
    Rcmd.key_file= "spec/testconfig.yml"
    expect(Rcmd.key_file).to match("spec/testconfig.yml")
  end
  
  it "requires command to be set", :mainlib => true do
    Rcmd.command= nil
    expect {Rcmd.run_command}.to raise_error(ArgumentError)
  end

 it "requires host_list to be set", :mainlib => true do
   Rcmd.command= "hostname -f"
   Rcmd.host_list= [ ]
   expect {Rcmd.run_command}.to raise_error(ArgumentError)
  end


 #
 # Database Tests
 #
 
 it "can connect to a database", :db => true do
   Rcmd::DB.override_config_file("spec/testconfig.yml")
   expect {Rcmd::DB.db_connect}.to_not raise_error
 end
 
 it "can perform hostname DB queries", :db => true do
   Rcmd::DB.override_config_file("spec/testconfig.yml")
   Rcmd::DB.db_connect
   expect(Rcmd::DB.query_by_hostname("node%")).not_to be_empty
   expect(Rcmd::DB.query_by_hostname("node%").count).to eq(20)
 end

 it "can perform type based DB queries", :db => true do
   Rcmd::DB.override_config_file("spec/testconfig.yml")
   Rcmd::DB.db_connect
   expect(Rcmd::DB.query_by_type("admin")).not_to be_empty
   expect(Rcmd::DB.query_by_type("compute").count).to eq(4)
 end
 
 it "can perform os based DB queries", :db => true do
   Rcmd::DB.override_config_file("spec/testconfig.yml")
   Rcmd::DB.db_connect
   expect(Rcmd::DB.query_by_os("slackware")).not_to be_empty
   expect(Rcmd::DB.query_by_os("rhel7").count).to eq(3)
 end
end

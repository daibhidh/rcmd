require "bundler/setup"
require "rcmd"
require "sqlite3"

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
  
  config.formatter = :documentation
end

begin  
  db = SQLite3::Database.new "testdb"
  db.execute "create table servers(id integer primary key autoincrement, hostname text not null, stype text not null, os_type text not null)"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node1', 'dev', 'rhel6')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node2', 'dev', 'rhel6')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node3', 'dev', 'rhel6')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node4', 'dev', 'rhel7')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node5', 'dev', 'rhel6')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node6', 'web', 'rhel6')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node7', 'web', 'rhel6')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node8', 'web', 'rhel7')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node9', 'web', 'rhel7')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node10', 'admin', 'ubuntu-LTS')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node11', 'admin', 'ubuntu-LTS')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node12', 'admin', 'ubuntu')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node13', 'admin', 'ubuntu')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node14', 'compute', 'debian')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node15', 'compute', 'debian')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node16', 'compute', 'debian')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node17', 'compute', 'debian')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node18', 'db', 'slackware')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node19', 'db', 'slackware')"
  db.execute "INSERT INTO servers (hostname, stype, os_type) VALUES ( 'node20', 'db', 'slackware')"
rescue SQLite3::Exception => e 
  puts "Exception occurred"
  puts e
end
